# lame-strapdata-ate

Provisioning de VM sur la lame strapdata ATE via un playbook ansible.

## Usage

La création de VM se fait en ajoutant un hôte dans le groupe `vm` l'inventaire ansible :

```ini
# file: hosts
...

[vm]
...
myVm ip_suffix=101 memory=1024 cpu=2 disk_size=20G
```

Parametres :
* `myVM` : le hostname de la vm
* `memory`: la ram en mo
* `disk_size`: la taille du disque
* `ip_suffix=X`: determine les addresses ip allouées :
    * eth0 172.16.91.X : réseau entreprise, vpn ATE/INEAT
    * eth1 10.12.213.X : réseau privé, pour l'interconnection des vm

Deploiement:
```bash
ansible-playbook playbook.yml -l myVm
```

## Back-end

### virtualisation

La lame fonctionne avec KVM et libvirt.

Le répertoire `/home/libvirt/managed` contient :
* `./templates` :  les templates des images
* `./images`: les images instances déployées (qcow2, iso)
* `./cloud_init`: les configurations cloud-init des instance

### Réseau

Interfaces:
* WAN: `em1` et `em2` redondés => `bond0` => vlan tagging `bond0.802`  => bridge `publicbr`
* LAN: p2p1 =>
    * vlan tagging `p2p1.2211` => bridge `rpcbr` (réseau entreprise)
    * vlan tagging `p2p1.2213` => brdige `intervr` (réseau privé)
* ISCSI: `p1p1` => `p1p1.552`

### Stockage

* /dev/sda (185G): ssd local
   * /dev/sda1: boot
   * /dev/sda2 : LVM cl_strapdata
        * root
        * swap
        * home
* /dev/sdb (500G): iscsi
    * /dev/sdb1: LVM vg_data (vm)

* /dev/sdc: iscsi failover
